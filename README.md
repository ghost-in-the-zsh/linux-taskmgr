# GNU/Linux Task Monitor
It's like a task manager, without the management.

## Description

This program can monitor the following:

* CPU utilization at the system and process levels
* Memory utilization at the system and process levels
* Disk I/O utilization at the system level
* Network I/O utilization at the system level
* Active TCP/UDP connections
* Application (process level) performance
* Tasks, open files, and network connections for specific users

It has been tested in Kubuntu 15.04 and 15.10.

## Data Sources
This monitor uses the `/proc` and `/sys` pseudo-filesystems as sources of data. The following
might be of particular interest:

* `/proc/cpuinfo`: CPU Model
* `/proc/stat`: CPU Usage
* `/proc/net/tcp`: TCP Connections (Transport Layer)
* `/proc/net/udp`: UDP Connections (Transport Layer)
* `/proc/net/dev`: Active Iface Discovery and Network Upload/Download Rates (IP Layer)
* `/proc/net/snmp`: Network Packet Statistics (IP Layer)
* `/proc/meminfo`: Overall Memory Usage
* `/proc/<pid>/*`: Files with per-Task Information
* `/sys/block/<dev>/stat`: Hard Drive Statistics (e.g. `sda`, `sdb`, etc.)

## Launching the Task Monitor

Use the `python3` interpreter on the `application.py` launcher to start the monitor. Get
help using the `-h` option. For example:

    $ python3 -B application.py -h
    usage: application.py [-h] [-i INTERVAL]

    A task monitor for the GNU/Linux platform.

    optional arguments:
    -h, --help            show this help message and exit
    -i INTERVAL, --interval INTERVAL
                            number of seconds to wait between updates (default: 3)

Similar to the `top` command, the interval can be specified with an argument, `-i` in our case.
The following example command sets intervals to 1 second:

    $ python3 -B application.py -i 1

## Dependencies

This program requires the following packages and utilities:

* `python3-pyqt4`: Python3 bindings for Qt4 in PyQt4 module
* `python3-pyqtgraph`: Python3 module for plotting graphs

The GUI depends on them for the widgets and other components. The `python3-pyqtgraph` module
has the following additional dependencies, which should be automatically installed for you
if you're using your distribution's package manager:

* `freeglut3`
* `libjs-jquery-ui`
* `python-matplotlib-data`
* `python3-dateutil`
* `python3-decorator`
* `python3-matplotlib`
* `python3-nose`
* `python3-numpy`
* `python3-opengl`
* `python3-pyparsing`
* `python3-pyqtgraph`
* `python3-scipy`
* `python3-tz`

Note that all of these are names for the Ubuntu distribution. You should find their equivalents
in whatever distribution you're using.

# Internal Messages
The bindings being used (i.e. PyQt4 and PyQtGraph) will *sometimes* display warnings during execution
or error messages while exiting. These messages are internal binding issues and not problems with my
client code. The application does work normally.

## Warning Messages
The warnings below is internal to `pyqtgraph`, which is used for the graphs in the system load tab.

    $ ./application.py -i 1
    /usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/AxisItem.py:841: RuntimeWarning: overflow encountered in double_scalars
    xScale = -bounds.height() / dif
    /usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/AxisItem.py:847: RuntimeWarning: invalid value encountered in double_scalars
    xRange = [x * xScale - offset for x in self.range]
    /usr/lib/python3/dist-packages/pyqtgraph/graphicsItems/AxisItem.py:871: RuntimeWarning: invalid value encountered in double_scalars
    x = (v * xScale) - offset

These are caused by an intentional decision on my part to *not* specify the vertical axis range in the
network upload/download graphs. By not specifying the range, the graph automatically adjusts its vertical
axis range depending on how high the graph itself gets plotted. Otherwise, the user would have to manually
zoom in/out of the graph to view it properly, which is annoying.

## Error Messages
I found the error message below is *sometimes* displayed when the user exits the application.
This problem *appears* to be internal to the PyQt4 binding itself and its interaction with
`glibc`[1](https://stackoverflow.com/questions/14897157/what-does-corrupted-double-linked-list-mean).

    $ ./application.py -i 1
    *** Error in `/usr/bin/python3': corrupted double-linked list (not small): 0x0000000000ebd450 ***
    [1]    12199 abort (core dumped)  ./application.py -i 1

# Misc
Based on user request, I had added the ability to resolve remote hostnames using reverse DNS lookup, but
it was extremely slow, so I removed it.
