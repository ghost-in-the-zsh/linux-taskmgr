#!/usr/bin/python3 -B

'''

'''
from abc import ABCMeta, abstractmethod


class Manager(metaclass=ABCMeta):
    '''A base class for manager classes.

    It defines a basic method that should be implemented by children.
    '''
    @abstractmethod
    def update(self, interval_secs, interval_cnt):
        if interval_secs <= 0:
            raise ValueError('interval seconds must be > 0')
        if interval_cnt <= 0:
            raise ValueError('interval count must be > 0')
