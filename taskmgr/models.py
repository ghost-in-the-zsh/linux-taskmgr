#!/usr/bin/python3 -B

'''

'''
import sys
from time import time

from PyQt4.QtCore import QObject, QTimer, pyqtSignal, pyqtSlot
from PyQt4.QtGui  import QApplication

from system.managers import TaskManager, NetworkManager
from system.user import User
from hardware.managers import CpuManager, MemoryManager, DiskManager
from ui.views import MainWindow
from taskmgr.utils import get_argument_parser


class Application(QObject):

    def __init__(self, args):
        super(Application, self).__init__()
        self._intervals = 1
        self._taskmgr   = TaskManager()
        self._cpumgr    = CpuManager()
        self._memmgr    = MemoryManager()
        self._dskmgr    = DiskManager()
        self._netmgr    = NetworkManager()
        self._user      = User()
        self._qapp      = QApplication(sys.argv)
        self._window    = MainWindow()
        self._timer     = QTimer()
        self._last_time = time()

        self._create_connections(args.interval)

        self._timer.timeout.emit()

    def run(self):
        self._timer.start()
        self._window.show()
        sys.exit(self._qapp.exec_())

    def _create_connections(self, interval_secs):
        timer = self._timer
        timer.setSingleShot(False)
        timer.setInterval(interval_secs * 1000)  # in msecs
        timer.timeout.connect(self.on_interval_elapsed)

        win  = self._window
        user = self._user
        user.update_success.connect(win.on_user_update_successful)
        user.update_failure.connect(win.on_user_update_failure)
        win.user_update_request.connect(self.on_user_update_requested)

    def on_interval_elapsed(self):
        now   = time()
        delta = now - self._last_time
        self._last_time = now

        self._cpumgr.update(delta, self._intervals)
        self._memmgr.update(delta, self._intervals)
        self._netmgr.update(delta, self._intervals)
        self._dskmgr.update(delta, self._intervals)

        time_secs = self._cpumgr.total_secs_user + self._cpumgr.total_secs_system
        self._taskmgr.update(time_secs, self._intervals, self._cpumgr.cpu_count)

        win = self._window
        win.update_sysload(self._cpumgr, self._memmgr, self._netmgr)
        win.update_tasks(self._taskmgr.tasks)
        win.update_disks(self._dskmgr.disks)
        win.update_net(self._netmgr)

        self._intervals += 1

    @pyqtSlot(str)
    def on_user_update_requested(self, username):
        self._user.update(username, self._taskmgr.tasks, self._netmgr.connections)
