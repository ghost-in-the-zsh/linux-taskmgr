#!/usr/bin/python3 -B

'''

'''
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def get_argument_parser():
    parser = ArgumentParser(
        description='A task monitor for the GNU/Linux platform.',
        formatter_class=ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        '-i', '--interval', dest='interval', default=3, type=int,
        metavar='INTERVAL', help='number of seconds to wait between updates'
    )
    return parser
