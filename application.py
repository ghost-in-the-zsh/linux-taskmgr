#!/usr/bin/python3 -B

'''

'''
from taskmgr.models import Application
from taskmgr.utils import get_argument_parser


if __name__ == '__main__':
    app = Application(get_argument_parser().parse_args())
    app.run()
