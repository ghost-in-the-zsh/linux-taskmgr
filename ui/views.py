#!/usr/bin/python3 -B

'''

'''
from PyQt4.QtCore import Qt, QObject, QTimer, pyqtSignal, pyqtSlot
from PyQt4.QtGui import QMainWindow, QTableWidgetItem, QPainter, QDialog
from PyQt4.uic import loadUi
import numpy as np
import pyqtgraph as pg

from system.user import User


class MainWindow(QMainWindow):
    '''The main view for the GUI.'''

    user_update_request = pyqtSignal(object)

    def __init__(self):
        super(MainWindow, self).__init__()
        self._start_time = pg.ptime.time()

        # Set up the user interface from Designer.
        loadUi('ui/mainwindow.ui', self)

        cpu_plot    = HistoryPlotWidget(self.cpu_usage_groupbox, 'CPU Usage History')
        mem_plot    = HistoryPlotWidget(self.mem_usage_groupbox, 'Memory Usage History')
        net_plot_up = HistoryPlotWidget(self.net_up_groupbox, 'Network Data Upload Rate')
        net_plot_dn = HistoryPlotWidget(self.net_down_groupbox, 'Network Data Download Rate')

        cpu_plot.showGrid(True, True)
        mem_plot.showGrid(True, True)
        net_plot_up.showGrid(True, True)
        net_plot_dn.showGrid(True, True)

        self._setup_usage_plotwidget(cpu_plot)
        self._setup_usage_plotwidget(mem_plot)
        self._setup_rate_plotwidget(net_plot_up)
        self._setup_rate_plotwidget(net_plot_dn)

        self.cpu_usage_plot    = cpu_plot
        self.mem_usage_plot    = mem_plot
        self.net_usage_plot_up = net_plot_up
        self.net_usage_plot_dn = net_plot_dn

        # insert the graphs above the data labels added in Designer
        self.cpu_usage_vlayout.insertWidget(0, cpu_plot)
        self.mem_usage_vlayout.insertWidget(0, mem_plot)
        self.net_up_vlayout.insertWidget(0, net_plot_up)
        self.net_down_vlayout.insertWidget(0, net_plot_dn)

        # workaround for Qt Designer bug which keeps setting
        # the horizontal header's visibility to false...
        self.task_tbl.horizontalHeader().setVisible(True)
        self.user_task_tbl.horizontalHeader().setVisible(True)
        self.find_user_btn.setEnabled(False)
        self.clear_user_btn.setEnabled(False)

        # set the button to enabled only when there's text,
        # pressing enter key when typing triggers the button's click slot,
        # and the button's clicked signal causes a request to update user data
        self.user_search_line.textChanged.connect(self.on_user_search_text_changed)
        self.user_search_line.returnPressed.connect(self.find_user_btn.click)
        self.find_user_btn.clicked.connect(self._on_user_update_requested)
        self.clear_user_btn.clicked.connect(self._on_user_clear_requested)
        self.menu_exit_action.triggered.connect(self.close)
        self.menu_about_action.triggered.connect(self._on_show_about_dialog)

    def update_sysload(self, cpumgr, memmgr, netmgr):
        delta = pg.ptime.time() - self._start_time

        # INFO: Tooltips should refresh text automatically as soon as the values
        #       are updated, but it might take more effort[1,2], especially
        #       when the docs point to a broken link for examples. Dropped.
        #
        #       [1] http://pyqt.sourceforge.net/Docs/PyQt4/qtooltip.html#details
        #       [2] https://forum.qt.io/topic/36855/solved-constantly-updating-tooltip-text-without-moving-the-mouse/7
        pw = self.cpu_usage_plot
        pw.update(cpumgr.curr_usage, delta)

        self.cpu_model_lbl.setText('<b>Model:</b> {}'.format(str(cpumgr)))
        self.cpu_usage_lbl.setText('<b>Current Usage:</b> {:.2f}%'.format(cpumgr.curr_usage))
        self.cpu_peak_lbl.setText('<b>Peak Min/Max/Avg:</b> {:.2f}%/{:.2f}%/{:.2f}%'.format(
            cpumgr.min_usage,
            cpumgr.max_usage,
            cpumgr.avg_usage
        ))
        self.cpu_ctxt_lbl.setText('<b>Context Switches:</b> {:,}/s'.format(cpumgr.context_rate))
        self.cpu_intr_lbl.setText('<b>Interrupts Serviced:</b> {:,}/s'.format(cpumgr.interrupt_rate))

        pw = self.mem_usage_plot
        pw.update(memmgr.total_usage, delta)
        self.mem_total_lbl.setText('<b>Memory:</b> {}'.format(str(memmgr)))
        self.mem_usage_lbl.setText('<b>Usage:</b> {:.2f}%'.format(memmgr.total_usage))

        pw = self.net_usage_plot_up
        pw.update(netmgr.upload_rate, delta)
        self.net_up_rate_lbl.setText('<b>{} Upload Rate:</b> {:.2f} KB/s'.format(netmgr.iface_name, netmgr.upload_rate))

        pw = self.net_usage_plot_dn
        pw.update(netmgr.download_rate, delta)
        self.net_down_rate_lbl.setText('<b>{} Download Rate:</b> {:.2f} KB/s'.format(netmgr.iface_name, netmgr.download_rate))


    def update_tasks(self, tasks):
        filter_text = self.task_search_line.text()
        if len(filter_text) > 0:
            filtered = [t for t in tasks if t.name.startswith(filter_text)]
            task_cnt = len(filtered)
            self._add_tasks(filtered, self.task_tbl)
        else:
            self._add_tasks(tasks, self.task_tbl)
            task_cnt = len(tasks)

        self.task_count_lbl.setText('<b>Total Tasks:</b> {:,}'.format(task_cnt))

    def update_disks(self, disks):
        tbl = self.disk_tbl
        tbl.setSortingEnabled(False)
        tbl.clearContents()
        tbl.setRowCount(len(disks))

        row = 0
        for d in disks:
            name   = QTableWidgetItem(d.name)
            total  = QTableWidgetItem()
            reads  = QTableWidgetItem()
            writes = QTableWidgetItem()

            total.setData(Qt.DisplayRole, '{:2.2f} GB'.format(d.total_space_gb))
            reads.setData(Qt.DisplayRole, '{:2.2f} MB/s'.format(d.read_rate_mbs))
            writes.setData(Qt.DisplayRole, '{:2.2f} MB/s'.format(d.write_rate_mbs))

            tbl.setItem(row, 0, name)
            tbl.setItem(row, 1, total)
            tbl.setItem(row, 2, reads)
            tbl.setItem(row, 3, writes)

            row += 1

        tbl.setSortingEnabled(True)

    def update_net(self, netmgr):
        self._update_tcp(netmgr.tcp)
        self._update_udp(netmgr.udp)
        self._update_inet(netmgr.inet)
        self._update_connections(netmgr.connections, self.net_addresses_tbl)
        self.conn_count_lbl.setText('<b>Total Connections:</b> {:,}'.format(len(netmgr.connections)))

    def _update_tcp(self, tcp):
        fmtstr = '{:,}'
        self.tcp_conn_open_lbl.setText(fmtstr.format(tcp.open_connections_cnt))
        self.tcp_conn_established_lbl.setText(fmtstr.format(tcp.established_connections_cnt))
        self.tcp_segs_recv_lbl.setText(fmtstr.format(tcp.segments_received_cnt))
        self.tcp_segs_sent_lbl.setText(fmtstr.format(tcp.segments_sent_cnt))

    def _update_udp(self, udp):
        fmtstr = '{:,}'
        self.udp_dgram_recv_lbl.setText(fmtstr.format(udp.dgrams_received_cnt))
        self.udp_dgram_sent_lbl.setText(fmtstr.format(udp.dgrams_sent_cnt))

    def _update_inet(self, inet):
        fmtstr = '{:,}'
        self.ip_pkts_req_lbl.setText(fmtstr.format(inet.packets_requested_cnt))
        self.ip_pkts_recv_lbl.setText(fmtstr.format(inet.packets_received_cnt))
        self.ip_pkts_delivered_lbl.setText(fmtstr.format(inet.packets_delivered_cnt))
        self.ip_pkts_discarded_in_lbl.setText(fmtstr.format(inet.packets_discarded_cnt_us))
        self.ip_pkts_discarded_out_lbl.setText(fmtstr.format(inet.packets_discarded_cnt_them))

    def _update_connections(self, conns, tbl):
        tbl.setSortingEnabled(False)
        tbl.clearContents()
        tbl.setRowCount(len(conns))

        row = 0
        for c in conns:
            tbl.setItem(row, 0, QTableWidgetItem(c.user))
            tbl.setItem(row, 1, QTableWidgetItem(c.local_host))
            tbl.setItem(row, 2, QTableWidgetItem(c.local_port))
            tbl.setItem(row, 3, QTableWidgetItem(c.remote_host))
            tbl.setItem(row, 4, QTableWidgetItem(c.remote_port))
            tbl.setItem(row, 5, QTableWidgetItem(c.proto))
            row += 1

        tbl.setSortingEnabled(True)

    @pyqtSlot(str)
    def on_user_search_text_changed(self, text):
        enabled = len(text.strip()) > 0
        self.find_user_btn.setEnabled(enabled)
        self.clear_user_btn.setEnabled(enabled)

    @pyqtSlot()
    def _on_user_update_requested(self):
        username = self.user_search_line.text()
        self.user_update_request.emit(username)

    @pyqtSlot()
    def _on_user_clear_requested(self):
        ui = self
        ui.user_task_tbl.clearContents()
        ui.user_task_tbl.setRowCount(0)
        ui.user_files_tbl.clearContents()
        ui.user_files_tbl.setRowCount(0)
        ui.user_net_addresses_tbl.clearContents()
        ui.user_net_addresses_tbl.setRowCount(0)
        ui.user_search_line.setText('')
        ui.user_task_count_lbl.setText('')
        ui.user_file_count_lbl.setText('')
        ui.user_conn_count_lbl.setText('')

    @pyqtSlot(User)
    def on_user_update_successful(self, user):
        self._add_tasks(user.tasks, self.user_task_tbl)
        openfiles = set()
        for t in user.tasks:
            openfiles |= t.open_files_set
        self._add_files(openfiles, self.user_files_tbl)
        self._update_connections(user.connections, self.user_net_addresses_tbl)
        self.user_task_count_lbl.setText('<b>Tasks:</b> {}'.format(len(user.tasks)))
        self.user_file_count_lbl.setText('<b>Files:</b> {}'.format(len(openfiles)))
        self.user_conn_count_lbl.setText('<b>Connections:</b> {}'.format(len(user.connections)))

    @pyqtSlot(str)
    def on_user_update_failure(self, errmsg):
        self._on_user_clear_requested()
        self.user_invalid_lbl.setText(errmsg)
        timer = QTimer()
        timer.setSingleShot(True)
        timer.setInterval(8000)  # in msecs
        timer.timeout.connect(self.on_invalid_user_warning_time_expired)
        timer.start()
        self._timer = timer

    @pyqtSlot()
    def on_invalid_user_warning_time_expired(self):
        self.user_invalid_lbl.setText('')
        del self._timer

    def _add_tasks(self, tasks, tbl):
        # INFO: Sorting is disabled before processing the rows and re-enabled afterwards
        #       as a workaround to a known problem with QTableWidget's sorting behavior
        #       where it would cause cells to be empty. See SO post:
        # https://stackoverflow.com/questions/7960505
        tbl.setSortingEnabled(False)

        tbl.clearContents()
        tbl.setRowCount(len(tasks))

        row = 0
        for t in tasks:
            name    = QTableWidgetItem(t.name)
            owner   = QTableWidgetItem(t.owner)
            cmdline = QTableWidgetItem(t.cmdline)

            # use numeric sorting for these columns
            tid  = QTableWidgetItem()
            tid.setData(Qt.DisplayRole, t.id)
            time = QTableWidgetItem()
            time.setData(Qt.DisplayRole, t.total_secs)
            cpu_used = QTableWidgetItem()
            cpu_used.setData(Qt.DisplayRole, '{:2.2f}%'.format(t.cpu_usage))
            mem_used = QTableWidgetItem()
            mem_used.setData(Qt.DisplayRole, '{:,} K'.format(t.memory_usage))

            tbl.setItem(row, 0, tid)
            tbl.setItem(row, 1, name)
            tbl.setItem(row, 2, owner)
            tbl.setItem(row, 3, time)
            tbl.setItem(row, 4, cpu_used)
            tbl.setItem(row, 5, mem_used)
            tbl.setItem(row, 6, cmdline)

            row += 1

        tbl.setSortingEnabled(True)

    def _add_files(self, filenames, tbl):
        tbl.setSortingEnabled(False)
        tbl.clearContents()
        tbl.setRowCount(len(filenames))

        row = 0
        for fname in filenames:
            tbl.setItem(row, 0, QTableWidgetItem(fname))
            row += 1

        tbl.setRowCount(row)
        tbl.setSortingEnabled(True)

    def _setup_usage_plotwidget(self, pw):
        pw.setLabel('left', 'Usage', units='%')
        pw.setLabel('bottom', 'Time', units='s')
        pw.setXRange(-30, 0)
        pw.setYRange(0, 100)
        pw.setRenderHints(QPainter.Antialiasing | QPainter.TextAntialiasing)

    def _setup_rate_plotwidget(self, pw):
        pw.setLabel('left', 'Rate', units='KB/s')
        pw.setLabel('bottom', 'Time', units='s')
        pw.setXRange(-30, 0)
        pw.setRenderHints(QPainter.Antialiasing | QPainter.TextAntialiasing)

    def _on_show_about_dialog(self):
        diag = AboutDialog()
        diag.show()
        diag.exec_()


class AboutDialog(QDialog):
    '''A dialog to display information about the program.'''
    def __init__(self):
        super(AboutDialog, self).__init__()
        loadUi('ui/aboutdialog.ui', self)


class HistoryPlotWidget(pg.PlotWidget):
    _SAMPLE_SIZE = 150
    _MAX_SAMPLES = 100

    def __init__(self, parent, title=None):
        super(HistoryPlotWidget, self).__init__(parent, title=title)
        self._curves     = []
        self._sample_cnt = 0
        self._data       = np.empty((self._SAMPLE_SIZE+1, 2))

    def update(self, usage_data, delta_time):
        for c in self._curves:
            c.setPos(-delta_time, 0)

        i = self._sample_cnt % self._SAMPLE_SIZE
        if i == 0:
            curve = self.plot()
            self._curves.append(curve)
            last_sample   = self._data[-1]
            self._data    = np.empty((self._SAMPLE_SIZE+1, 2))
            self._data[0] = last_sample

            while len(self._curves) > self._MAX_SAMPLES:
                c = self._curves.pop(0)
                self.removeItem(c)
        else:
            curve = self._curves[-1]

        self._data[i+1, 0] = delta_time
        self._data[i+1, 1] = usage_data
        curve.setData(x=self._data[:i+2, 0], y=self._data[:i+2, 1])
        self._sample_cnt += 1
