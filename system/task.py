#!/usr/bin/python3 -B

'''

'''
from os import listdir, readlink

from PyQt4.QtCore import QObject, pyqtSignal
from hardware.utils import cpu_usage_aggregate, jiffs_to_secs


class Task(QObject):
    '''A task running as a system process.

    A task is uniquely identified by its process ID.
    '''
    # using object as workaround; Task is not yet defined for pyqtSignal
    ready  = pyqtSignal(object)
    exited = pyqtSignal(object)

    def __init__(self, tskmgr, pid):
        super(Task, self).__init__()
        self._pid   = pid
        self._path  = '/proc/{}'.format(pid)

        self.ready.connect(tskmgr.on_task_ready)
        self.exited.connect(tskmgr.on_task_exited)

        self._cpu_usage = 0
        try:
            self._name      = self._get_name()
            self._owner     = self._get_owner()
            self._cmdline   = self._get_cmdline()
            self._curr_secs = self._get_time_secs()
            self._last_secs = 0
            self._mem_usage = self._get_memory_kb()
            self.ready.emit(self)
        except (FileNotFoundError, ProcessLookupError):
            self.exited.emit(self)

    @property
    def id(self):
        return self._pid

    @property
    def name(self):
        return self._name

    @property
    def owner(self):
        return self._owner

    @property
    def total_secs(self):
        return self._curr_secs

    @property
    def cpu_usage(self):
        return self._cpu_usage

    @property
    def memory_usage(self):
        return self._mem_usage

    @property
    def cmdline(self):
        return self._cmdline

    @property
    def open_files_set(self):
        excluded = ('/pts/', 'socket:', 'pipe:', 'anon_inode:')
        def keep(name):
            for e in excluded:
                if e in name:
                    return False # shouln't keep file
            return True

        fd_path = '{}/fd'.format(self._path)
        fd_list = [fd for fd in listdir(fd_path) if int(fd) > 2] # skip stdin/out/err in range [0..2]
        files   = []
        for fd in fd_list:
            try:
                files.append(readlink('{}/{}'.format(fd_path, fd)))
            except FileNotFoundError:
                # file descriptor could've been removed by now
                pass

        # filter out terminals, sockets, etc.
        return set(filter(keep, files))

    def update(self, interval_secs, interval_cnt, cpu_count):
        try:
            # check the name to see if the PID we had assigned
            # is being reused with another process
            name = self._get_name()
            if name != self._name:
                # PID got recycled, so we died
                self.exited.emit(self)
                return

            self._last_secs = self._curr_secs
            self._curr_secs = self._get_time_secs()

            # div by number of cpus to get usage % relative to full
            # system (all cores), and not to single core
            self._cpu_usage = cpu_usage_aggregate(
                                self._curr_secs - self._last_secs,
                                interval_secs, interval_cnt, self._cpu_usage
                            ) / cpu_count

            self._mem_usage = self._get_memory_kb()
        except (FileNotFoundError, ProcessLookupError):
            self.exited.emit(self)

    def _get_name(self):
        # get 2nd column and strip parentheses
        # e.g.: 10145 (zsh) S 29692 10145 ...
        with open('{}/{}'.format(self._path, 'stat')) as f:
            return f.readline().split()[1][1:-1]

    def _get_time_secs(self):
        # get utime & stime columns
        with open('{}/{}'.format(self._path, 'stat')) as f:
            times = f.readline().split()[13:15]
            return jiffs_to_secs(int(times[0]) + int(times[1]))

    def _get_owner(self):
        try:
            with open('{}/{}'.format(self._path, 'environ')) as f:
                lines = f.readline().split('\x00')
                return next(e for e in lines if e.startswith('LOGNAME')).split('=')[1]
        except (PermissionError, StopIteration):
            return None

    def _get_cmdline(self):
        with open('{}/{}'.format(self._path, 'cmdline')) as f:
            # replace null delimiters and remove extra spaces
            return f.readline().replace('\x00', ' ').strip()

    def _get_memory_kb(self):
        with open('{}/{}'.format(self._path, 'status')) as f:
            try:
                # not all tasks have this status line or report memory page
                # info in the statm file (e.g. kmpath_handlerd, kworker, etc)
                return int([l for l in f if l.startswith('VmRSS')][0].split()[1])
            except IndexError:
                return 0


    def __str__(self):
        return '{}/{}'.format(self._name, self._pid)
