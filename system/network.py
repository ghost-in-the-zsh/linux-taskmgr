#!/usr/bin/python3 -B

'''

'''
from pwd import getpwuid


class Tcp(object):

    def __init__(self, stat):
        self._parse_stats(stat)

    @property
    def open_connections_cnt(self):
        return self._conns_open

    @property
    def established_connections_cnt(self):
        return self._conns_estab

    @property
    def segments_received_cnt(self):
        return self._segs_rcvd

    @property
    def segments_sent_cnt(self):
        return self._segs_sent

    def update(self, stat, interval_secs, interval_cnt):
        self._parse_stats(stat)

    def _parse_stats(self, stat):
        # Tcp: RtoAlgorithm RtoMin RtoMax MaxConn ActiveOpens PassiveOpens AttemptFails EstabResets CurrEstab InSegs OutSegs RetransSegs InErrs OutRsts InCsumErrors
        # Tcp: 1 200 120000 -1 3553 191 14 183 10 40852311 76481973 38206 23 6485 1
        fields = [int(v) for v in stat.split()[1:]]
        self._conns_open    = fields[4]
        self._conns_estab   = fields[8]
        self._segs_rcvd     = fields[9]
        self._segs_sent     = fields[10]


class Udp(object):

    def __init__(self, stat):
        self._parse_stats(stat)

    @property
    def dgrams_received_cnt(self):
        return self._dgrams_rcvd

    @property
    def dgrams_sent_cnt(self):
        return self._dgrams_sent

    def update(self, stat, interval_secs, interval_cnt):
        self._parse_stats(stat)

    def _parse_stats(self, stat):
        # Udp: InDatagrams NoPorts InErrors OutDatagrams RcvbufErrors SndbufErrors InCsumErrors IgnoredMulti
        # Udp: 80322 176 0 80174 0 0 0 0
        fields = [int(v) for v in stat.split()[1:5:3]]
        self._dgrams_rcvd = fields[0]
        self._dgrams_sent = fields[1]


class Inet(object):

    def __init__(self, stat):
        self._parse_stats(stat)

    @property
    def packets_requested_cnt(self):
        return self._requested

    @property
    def packets_received_cnt(self):
        return self._received

    @property
    def packets_delivered_cnt(self):
        return self._delivered

    @property
    def packets_discarded_cnt_us(self):
        return self._in_discarded

    @property
    def packets_discarded_cnt_them(self):
        return self._out_discarded

    def update(self, stat, interval_secs, interval_cnt):
        self._parse_stats(stat)

    def _parse_stats(self, stat):
        # Ip: Forwarding DefaultTTL InReceives InHdrErrors InAddrErrors ForwDatagrams InUnknownProtos InDiscards InDelivers OutRequests OutDiscards OutNoRoutes ReasmTimeout ReasmReqds ReasmOKs ReasmFails FragOKs FragFails FragCreates
        # Ip: 2 64 40931963 0 3 0 0 0 40931960 38516511 80 41 0 0 0 0 0 0 0
        fields = [int(v) for v in stat.split()[3:12]]
        self._received      = fields[0]
        self._in_discarded  = fields[5]
        self._delivered     = fields[6]
        self._requested     = fields[7]
        self._out_discarded = fields[8]


class InetConnection(object):

    def __init__(self, line):
        self._lhost = None
        self._lport = None
        self._rhost = None
        self._rport = None
        self._user  = None
        self._parse_line(line)

    @property
    def local_host(self):
        return self._lhost

    @property
    def local_port(self):
        return self._lport

    @property
    def remote_host(self):
        return self._rhost

    @property
    def remote_port(self):
        return self._rport

    @property
    def user(self):
        return self._user

    @property
    def proto(self):
        raise NotImplementedError()

    def _parse_line(self, line):
        # sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode
        # 10: 0100007F:44C0 00000000:0000 0A 00000000:00000000 00:00000000 00000000  1000        0 31805 1 0000000000000000 100 0 0 10 0
        fields  = line.split()[1:8]
        l_addr  = fields[0].split(':')
        r_addr  = fields[1].split(':')

        self._lhost = self._get_ip(l_addr[0])
        self._lport = self._hex2dec(l_addr[1])
        self._rhost = self._get_ip(r_addr[0])
        self._rport = self._hex2dec(r_addr[1])
        self._user  = getpwuid(int(fields[6]))[0]

    def _hex2dec(self, s):
        return str(int(s, 16))

    def _get_ip(self, s):
        h2s = self._hex2dec
        ip  = [h2s(s[6:8]), h2s(s[4:6]), h2s(s[2:4]), h2s(s[0:2])]
        return '.'.join(ip)


class TcpConnection(InetConnection):
    def __init__(self, line):
        super(TcpConnection, self).__init__(line)

    @property
    def proto(self):
        return 'TCP'


class UdpConnection(InetConnection):
    def __init__(self, line):
        super(UdpConnection, self).__init__(line)

    @property
    def proto(self):
        return 'UDP'
