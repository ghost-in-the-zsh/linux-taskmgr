#!/usr/bin/python3 -B

'''

'''
from os import listdir

from PyQt4.QtCore import QObject, pyqtSlot
from taskmgr.base import Manager
from system.task import Task
from system.network import Tcp, Udp, Inet, TcpConnection, UdpConnection


class TaskManager(QObject):

    def __init__(self):
        super(TaskManager, self).__init__()
        # make sure fields exist before slots are entered
        self._curr_tasks = dict()
        # tasks that died during an interval and are scheduled for removal
        self._ghosts = []

        # last measured overall CPU time
        self._last_cpu_secs = 0

        self._check_new_tasks()

    def update(self, cpu_time_secs, interval_cnt, cpu_count):
        if interval_cnt <= 1:
            self._last_cpu_secs = cpu_time_secs
            return

        self._check_new_tasks()

        # tasks may emit the exited signal; see on_task_exited
        for task in self._curr_tasks.values():
            task.update(cpu_time_secs - self._last_cpu_secs, interval_cnt, cpu_count)

        self._last_cpu_secs = cpu_time_secs
        for ghost in self._ghosts:
            try:
                del self._curr_tasks[ghost.id]
            except KeyError:
                pass

        self._ghosts.clear()

    @property
    def tasks(self):
        return self._curr_tasks.values()

    @pyqtSlot(Task)
    def on_task_ready(self, task):
        # this approach should make it easier to add individual tasks
        # as they get created during interval updates
        self._curr_tasks[task.id] = task

    @pyqtSlot(Task)
    def on_task_exited(self, ghost):
        # this slot is invoked while iterating the task dictionary
        # and must be scheduled for removal to avoid modifying
        # a dictionary during iteration; causes RuntimeError
        self._ghosts.append(ghost)

    def _check_new_tasks(self):
        # numeric directories under /proc are process IDs
        # tasks emit a ready signal; see on_task_ready
        pids = {int(pid) for pid in listdir('/proc') if pid.isdigit() and int(pid) not in self._curr_tasks}
        for pid in pids:
            Task(self, pid)


class NetworkManager(Manager):

    def __init__(self):
        (name, up, down) = self._discover_iface()
        self._iface     = name

        self._prev_sent = up
        self._prev_rcvd = down

        self._up_rate   = 0
        self._down_rate = 0

        data = self._get_netstack_stats()
        self._inet  = Inet(data[0])
        self._tcp   = Tcp(data[1])
        self._udp   = Udp(data[2])

        self._conns = self._get_connections()

    @property
    def tcp(self):
        return self._tcp

    @property
    def udp(self):
        return self._udp

    @property
    def inet(self):
        return self._inet

    @property
    def iface_name(self):
        return self._iface

    @property
    def connections(self):
        return self._conns

    @property
    def upload_rate(self):
        return self._up_rate / 1024

    @property
    def download_rate(self):
        return self._down_rate / 1024

    def update(self, interval_secs, interval_cnt):
        super(NetworkManager, self).update(interval_secs, interval_cnt)
        data = self._get_netstack_stats()
        self._inet.update(data[0], interval_secs, interval_cnt)
        self._tcp.update(data[1], interval_secs, interval_cnt)
        self._udp.update(data[2], interval_secs, interval_cnt)

        if self._iface is not None:
            data = self._get_usage_rate_stats(self._iface)
            curr_sent = data[0]
            curr_rcvd = data[1]

            self._up_rate   = (curr_sent - self._prev_sent) / interval_secs
            self._down_rate = (curr_rcvd - self._prev_rcvd) / interval_secs

            self._prev_sent = curr_sent
            self._prev_rcvd = curr_rcvd

    def _get_connections(self):
        with open('/proc/net/tcp') as f:
            tcp = [TcpConnection(line) for line in f.readlines()[1:]]

        with open('/proc/net/udp') as f:
            udp = [UdpConnection(line) for line in f.readlines()[1:]]

        return tcp + udp

    def _get_netstack_stats(self):
        with open('/proc/net/snmp') as f:
            # We filter stats for areas of interest and remove the headers for each
            # matched pair. The file has a header line and then the actual data in the
            # next. Since headers are in even-numbered positions, we slice the list
            # to select all odd-numbered elements.
            return [l for l in f if l.startswith('Ip:') or l.startswith('Tcp:') or l.startswith('Udp:')][1::2]

    def _discover_iface(self):
        ifaces = []
        try:
            with open('/proc/net/dev') as f:
                ifaces = [l.split(':')[0].strip() for l in f if 'eth' in l or 'wlan' in l]
        except IndexError:
            # no network interfaces found
            return (None, 0, 0)

        # choose the first iface to show activity
        for iface in ifaces:
            vals = self._get_usage_rate_stats(iface)
            if vals[0] > 0 or vals[1] > 0:
                # found network activity for iface
                return (iface, vals[0], vals[1])

        # no active network interface found
        return (None, 0, 0)

    def _get_usage_rate_stats(self, iface):
        with open('/proc/net/dev') as f:
            # Must get the 'bytes' for Receive and Transmit from sample below:
            #
            # Inter-|   Receive                                                |  Transmit
            #  face |bytes    packets errs drop fifo frame compressed multicast|bytes    packets errs drop fifo colls carrier compressed
            #   eth0: 8453173317 9664481    0    0    0     0          0         0 12493612636 10514784    0    0    0     0       0          0
            #     lo: 13019423   61827    0    0    0     0          0         0 13019423   61827    0    0    0     0       0          0
            #
            fields = [l.strip() for l in f if iface in l][0].split(':')

        return [int(v) for v in fields[1].split()[0:9:8]] # get 2 elements 0 & 9, skipping 8 in middle
