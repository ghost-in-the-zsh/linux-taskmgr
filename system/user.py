#!/usr/bin/python3 -B

'''

'''
from PyQt4.QtCore import QObject, pyqtSignal


class User(QObject):
    '''
    '''
    update_success = pyqtSignal(object)
    update_failure = pyqtSignal(str)

    def __init__(self):
        super(User, self).__init__()
        self._name  = None
        self._tasks = []
        self._conns = []

    @property
    def name(self):
        return self._name

    @property
    def tasks(self):
        return self._tasks

    @property
    def connections(self):
        return self._conns

    def update(self, name, tasks, conns):
        self._name  = name
        self._tasks = [t for t in tasks if t.owner == name]
        self._conns = [c for c in conns if c.user == name]

        if len(self._tasks) > 0 or len(self._conns) > 0:
            self.update_success.emit(self)
        else:
            self.update_failure.emit("'{}' seems to have no active sessions or does not exist".format(name))

    def __str__(self):
        return self._name
