#!/usr/bin/python3 -B

'''

'''
from PyQt4.QtCore import QObject, QTimer, pyqtSignal, pyqtSlot

from hardware.utils import jiffs_to_secs, cpu_usage_interval, cpu_usage_aggregate


class Cpu(object):
    '''Holds stats for a single CPU.

    This model relies on the /proc/stat file.
    '''
    def __init__(self):
        self._name = None

        self._curr_user_secs = 0
        self._curr_sys_secs  = 0
        self._curr_idle_secs = 0

        self._prev_user_secs = 0
        self._prev_sys_secs  = 0
        self._prev_idle_secs = 0

        self._min_usage = None
        self._max_usage = None
        self._avg_usage = 0    # allows initial calculation
        self._curr_usage= 0

    @property
    def name(self):
        return self._name

    @property
    def user_secs(self):
        '''Intervalized time (in seconds) this CPU has spent in user mode.'''
        return self._curr_user_secs

    @property
    def system_secs(self):
        '''Intervalized time (in seconds) this CPU has spent in system mode.'''
        return self._curr_sys_secs

    @property
    def idle_secs(self):
        '''Intervalized time (in seconds) this CPU has spent idle.'''
        return self._curr_idle_secs

    @property
    def min_usage(self):
        '''The lowest average utilization value recorded.'''
        return self._min_usage if self._min_usage else 0

    @property
    def avg_usage(self):
        '''The current average utilization value.'''
        return self._avg_usage

    @property
    def curr_usage(self):
        return self._curr_usage

    @property
    def max_usage(self):
        '''The highest average utilization value recorded.'''
        return self._max_usage if self._max_usage else 0

    def update(self, statline, interval_secs, interval_count):
        self._parse_stats(statline)

        # skip bogus data from 1st interval reading
        # on min/avg/max cpu% utilization
        if interval_count > 1:
            self._update_usage(interval_secs, interval_count)

        self._prev_user_secs = self._curr_user_secs
        self._prev_sys_secs  = self._curr_sys_secs
        self._prev_idle_secs = self._curr_idle_secs

    def _parse_stats(self, stat):
        # 'cpu0 2252389 17960 258219 27981991 284032 915 59696 0 0 0'
        try:
            fields = [int(f) if f.isdigit() else f for f in stat.split()[:5]]
            if self._name is None:
                self._name = fields[0]
            self._curr_user_secs = jiffs_to_secs(fields[1])
            self._curr_sys_secs  = jiffs_to_secs(fields[3])
            self._curr_idle_secs = jiffs_to_secs(fields[4])
        except:
            raise ValueError('invalid cpu stat format?: {}'.format(stat))

    def _update_usage(self, interval_secs, interval_cnt):
        user_delta_secs = self._curr_user_secs - self._prev_user_secs
        sys_delta_secs  = self._curr_sys_secs  - self._prev_sys_secs

        self._curr_usage= cpu_usage_interval(
                            user_delta_secs + sys_delta_secs,
                            interval_secs
                        )
        self._avg_usage = cpu_usage_aggregate(
                            user_delta_secs + sys_delta_secs,
                            interval_secs,
                            interval_cnt,
                            self._avg_usage
                        )

        if self._min_usage is None or self._avg_usage < self._min_usage:
            self._min_usage = self._avg_usage

        if self._max_usage is None or self._avg_usage > self._max_usage:
            self._max_usage = self._avg_usage

    def __str__(self):
        return '{} usr/sys/idle = {}/{}/{}'.format(
            self._name,
            self._curr_user_secs,
            self._curr_sys_secs,
            self._curr_idle_secs
        )


class Disk(QObject):
    '''Holds stats for a single disk drive.

    This model relies on the /sys/block/<dev>/stat file.
    '''
    # disk capacity is reported in terms of 512-byte blocks
    # so we have to multiply to get the actual size in bytes
    _BLOCK_SIZE = 512
    _1_GB       = 2**30
    _1_MB       = 2**20
    ready       = pyqtSignal(object)
    removed     = pyqtSignal(object)

    def __init__(self, diskmgr, path):
        super(Disk, self).__init__()
        self.ready.connect(diskmgr.on_disk_ready)
        self.removed.connect(diskmgr.on_disk_removed)

        self._path          = path
        self._name          = path.split('/')[-1]
        self._total_gb      = self._get_total_size()
        self._blks_read     = 0
        self._blks_written  = 0
        self._read_rate     = 0
        self._write_rate    = 0

        # a disk with zero capacity may be a media reader with no actual media
        # or some other unmounted device, so we emit ready when we get a capacity
        # value only
        if self._total_gb > 0:
            self.ready.emit(self)

    @property
    def name(self):
        return self._name

    @property
    def total_space_gb(self):
        return self._total_gb

    @property
    def read_rate_mbs(self):
        return self._read_rate

    @property
    def write_rate_mbs(self):
        return self._write_rate

    def update(self, interval_secs, interval_count):
        try:
            (read_cnt, write_cnt) = self._get_rw_stats()
        except FileNotFoundError:
            self.removed.emit(self)
            return

        if interval_count > 1:
            # MBytes/sec
            mbs = self._BLOCK_SIZE / self._1_MB
            self._read_rate  = (read_cnt  - self._blks_read)    * mbs / interval_secs
            self._write_rate = (write_cnt - self._blks_written) * mbs / interval_secs

        self._blks_read     = read_cnt
        self._blks_written  = write_cnt

    def _get_total_size(self):
        with open('{}/{}'.format(self._path, 'size')) as f:
            return int(next(f)) * self._BLOCK_SIZE / self._1_GB

    def _get_rw_stats(self):
        # Format:
        # 240057    45606 15852728  4787064   184906   186427 18008418  9438328        0  1846916 14225076
        #   0         1      2          3       4         5      6
        # 0 = reads completed,  2 = # of blocks read
        # 4 = writes completed, 6 = # of blocks written
        with open('{}/{}'.format(self._path, 'stat')) as f:
            stats = [int(v) for v in next(f).split()[:7]]
        read_cnt    = stats[2]  # in blocks
        write_cnt   = stats[6]
        return (read_cnt, write_cnt)

    def __str__(self):
        return '{}: {}'.format(self._name, self._path)
