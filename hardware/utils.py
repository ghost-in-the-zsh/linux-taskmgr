#!/usr/bin/python3 -B

'''

'''

_TICKS_PER_SEC = 100  # os.sysconf_names['SC_CLK_TCK'] gives wrong value?...


def jiffs_to_secs(time_jiffies):
    '''Convert time from jiffies to seconds.'''
    return time_jiffies / _TICKS_PER_SEC


def cpu_usage_aggregate(delta_secs, int_secs, int_cnt, prev_avg):
    '''Calculates the aggregate utilization of a CPU, as a percentage.

    The calculated average is based on accumulated averages from previous
    intervals.

    delta_secs  : Combined time spent in user and system modes during an interval, in seconds.
    int_secs    : Measured length of the current interval, in seconds.
    int_cnt     : The number of intervals measured since the beginning.
    prev_avg    : The calculated average utilization for the previous interval, as
                    a percentage.
    '''
    if int_secs == 0:
        int_secs = 1
    interval_usage = cpu_usage_interval(delta_secs, int_secs)
    return (int_cnt * prev_avg + interval_usage) / (int_cnt + 1)


def cpu_usage_interval(delta_secs, int_secs):
    '''Calculates the utilization of a CPU during a given interval, as a percentage.

    The current average is based on the measured time between two intervals.

    delta_secs  : Combined time spent in user and system modes during an interval, in seconds.
    int_secs    : Measured length of the current interval, in seconds.
    '''
    return delta_secs / int_secs * 100
