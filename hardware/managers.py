#!/usr/bin/python3 -B

'''

'''
from os import listdir

from taskmgr.base import Manager
from hardware.devices import Cpu, Disk


class CpuManager(Manager):
    '''A manager class for Cpu instances.

    The manager is responsible for obtaining the statistics from
    the OS and managing the lifecycle of each CPU instance. It also
    reports the summary/overall information, leaving the per-CPU data
    to the Cpu models.
    '''
    def __init__(self):
        self._cpulist   = None
        self._model     = self._get_cpu_model()
        self._prev_ctxts= 0
        self._prev_intrs= 0
        self._ctxt_rate = 0
        self._intr_rate = 0

    def update(self, interval_secs, interval_cnt):
        super(CpuManager, self).update(interval_secs, interval_cnt)
        stats = self._read_stats()

        # skip context switches and serviced interrupts
        # on stats
        if not self._cpulist:
            self._cpulist = [Cpu() for _ in range(len(stats) - 2)]

        # ignore last 2 stat lines with context switches and interrupts
        for cpu, stat in zip(self._cpulist, stats[:-2]):
            cpu.update(stat, interval_secs, interval_cnt)

        curr_ctxts = int(stats[-1].split()[1])
        curr_intrs = int(stats[-2].split()[1])

        self._ctxt_rate = int((curr_ctxts - self._prev_ctxts) / interval_secs)
        self._intr_rate = int((curr_intrs - self._prev_intrs) / interval_secs)

        self._prev_ctxts = curr_ctxts
        self._prev_intrs = curr_intrs

    @property
    def cpu_count(self):
        # the 1st CPU instance holds summary data
        # and is not a real CPU
        return len(self._cpulist) - 1 if self._cpulist else None

    @property
    def total_secs_user(self):
        return self._cpulist[0].user_secs if self._cpulist else None

    @property
    def total_secs_system(self):
        return self._cpulist[0].system_secs if self._cpulist else None

    @property
    def total_secs_idle(self):
        return self._cpulist[0].idle_secs if self._cpulist else None

    @property
    def curr_usage(self):
        return self._cpulist[0].curr_usage / (len(self._cpulist) - 1) if self._cpulist else None

    @property
    def min_usage(self):
        return self._cpulist[0].min_usage / (len(self._cpulist) - 1) if self._cpulist else None

    @property
    def avg_usage(self):
        return self._cpulist[0].avg_usage / (len(self._cpulist) - 1) if self._cpulist else None

    @property
    def max_usage(self):
        return self._cpulist[0].max_usage / (len(self._cpulist) - 1) if self._cpulist else None

    @property
    def context_rate(self):
        return self._ctxt_rate

    @property
    def interrupt_rate(self):
        return self._intr_rate

    def _read_stats(self):
        with open('/proc/stat') as f:
            return [
                l.rstrip() for l in f
                if l.startswith('cpu')
                or l.startswith('ctxt')
                or l.startswith('intr')
            ]

    def _get_cpu_model(self):
        with open('/proc/cpuinfo') as f:
            for l in f:
                # model name      : Intel(R) Core(TM) i7 CPU         950  @ 3.07GHz
                if l.startswith('model name'):
                    return ' '.join(l.rstrip().split(':')[1].split())

    def __str__(self):
        return '{} x{}'.format(self._model, self.cpu_count)


class MemoryManager(Manager):
    '''A manager class for system memory.

    This class does not have an associated Memory model class to manage
    because we're not looking at individual memory modules. Instead, it
    gets the data from the /proc/meminfo file.
    '''
    def __init__(self):
        # total usable RAM
        self._total_mem  = None

        self._curr_avail = None
        self._curr_free  = None
        self._curr_used  = None
        self._curr_usage = None

    def update(self, interval_secs, interval_cnt):
        super(MemoryManager, self).update(interval_secs, interval_cnt)
        stats = self._read_stats()

        # convert values from KB to GBs, not GiBs
        if not self._total_mem:
            self._total_mem  = stats[0] / 1000000

        self._curr_free  = stats[1] / 1000000
        self._curr_avail = stats[2] / 1000000
        self._curr_used  = self._total_mem - self._curr_avail
        self._curr_usage = self._curr_used / self._total_mem * 100

    @property
    def total_mem(self):
        return self._total_mem

    @property
    def avail_mem(self):
        return self._curr_avail

    @property
    def used_mem(self):
        return self._curr_used

    @property
    def total_usage(self):
        return self._curr_usage

    def _read_stats(self):
        with open('/proc/meminfo') as f:
            return [int(next(f).split()[1]) for _ in range(3)]

    def __str__(self):
        return '{:2.1f} GB of {:2.1f} GB'.format(
            self.used_mem,
            self.total_mem
        )


class DiskManager(Manager):
    '''A manager class for Disk instances.

    The manager is responsible for obtaining the statistics from
    the OS and managing the lifecycle of each Disk instance. It also
    reports the summary/overall information, leaving the per-Disk data
    to the Disk models.

    Disk data is retrieved from the /sys/block/<dev>/stat file, not
    /proc/diskstats, as the former is faster and more efficient. See
    Linux source file Documentation/iostats.txt for more.
    '''
    _ROOT = '/sys/block'

    def __init__(self):
        paths = {
            '{}/{}'.format(self._ROOT, name) for name in listdir(self._ROOT) if name.startswith('sd') or name.startswith('hd')
        }
        self._disks = dict()
        self._ghosts= []

        # Disks emit ready signal to add themselves when valid
        for path in paths:
            Disk(self, path)

    @property
    def disks(self):
        return self._disks.values()

    def update(self, interval_secs, interval_cnt):
        super(DiskManager, self).update(interval_secs, interval_cnt)
        for disk in self._disks.values():
            disk.update(interval_secs, interval_cnt)

        for ghost in self._ghosts:
            try:
                del self._disks[ghost.name]
            except KeyError:
                pass
        self._ghosts.clear()

    # HACK: This method should be a pyqtSlot, but there's a metaclass conflict
    #       when inheriting from both Manager (an ABC) and QObject. Still, I was
    #       able to get away with connecting a signal from Disk to this 'slot' method.
    def on_disk_ready(self, disk):
        self._disks[disk.name] = disk

    # HACK: This method should be a pyqtSlot, but there's a metaclass conflict
    #       when inheriting from both Manager (an ABC) and QObject. Still, I was
    #       able to get away with connecting a signal from Disk to this 'slot' method.
    def on_disk_removed(self, disk):
        self._ghosts.append(disk)
